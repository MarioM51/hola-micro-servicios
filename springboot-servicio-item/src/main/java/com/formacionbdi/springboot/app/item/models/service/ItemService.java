package com.formacionbdi.springboot.app.item.models.service;

import java.util.List;

import com.formacionbdi.springboot.app.item.models.Item;
import com.marioCorps.carritocommons.model.Producto;

public interface ItemService {

	List<Item> findAll();
	Item findById(Long id, Integer cantidad);
	
	Producto save(Producto productoIn);
	
	Producto update(Producto productoIn, Long id);
	
	void delete(Long id);
}
