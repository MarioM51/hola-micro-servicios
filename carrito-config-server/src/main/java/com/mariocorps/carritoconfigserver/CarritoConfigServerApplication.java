package com.mariocorps.carritoconfigserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@EnableConfigServer
@SpringBootApplication
public class CarritoConfigServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(CarritoConfigServerApplication.class, args);
	}

}
