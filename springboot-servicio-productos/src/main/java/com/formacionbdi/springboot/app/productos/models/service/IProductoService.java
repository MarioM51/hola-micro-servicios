package com.formacionbdi.springboot.app.productos.models.service;

import java.util.List;

import com.marioCorps.carritocommons.model.Producto;

public interface IProductoService {

	List<Producto> findAll();
	
	Producto findById(Long id);
	
	Producto save(Producto p);
	
	void deleteById(Long id);
	
	
}
