INSERT INTO usuarios (username, password, enabled, nombre, apellido, email) VALUES ('andres','$2a$10$3e7kzk.vBw2pu6c7sfhWVuISv/rTPb1ohVXV4/v2ek3Z1bUA6uzvG',true, 'Andres', 'Guzman','profesor@bolsadeideas.com');
INSERT INTO usuarios (username, password, enabled, nombre, apellido, email) VALUES ('admin','$2a$10$OcPUNXl9W1nZMbaNm0dON.ywVCi4NSZ8fbHpZJrE0K2BImQFkaS7u',true, 'John', 'Doe','jhon.doe@bolsadeideas.com');

INSERT INTO roles (nombre) VALUES ('ROLE_USER');
INSERT INTO roles (nombre) VALUES ('ROLE_ADMIN');

INSERT INTO usuarios_roles (usuario_id, roles_id) VALUES (1, 1);
INSERT INTO usuarios_roles (usuario_id, roles_id) VALUES (2, 2);
INSERT INTO usuarios_roles (usuario_id, roles_id) VALUES (2, 1);