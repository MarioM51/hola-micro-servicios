package com.marioCorps.carritoclientes.model.Dao;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import com.marioCorps.carritocommons.model.Usuario;

@RepositoryRestResource(path="usaurios")
public interface UsuarioDao extends PagingAndSortingRepository<Usuario, Long>{
	
	@RestResource(path="filtrar")
	public Usuario findByUsername(@Param("nick") String username);
}
