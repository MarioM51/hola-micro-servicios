package com.marioCorps.carritoclientes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication
@EntityScan("com.marioCorps.carritocommons.model")
public class CarritoClientesApplication {

	public static void main(String[] args) {
		SpringApplication.run(CarritoClientesApplication.class, args);
	}

}
