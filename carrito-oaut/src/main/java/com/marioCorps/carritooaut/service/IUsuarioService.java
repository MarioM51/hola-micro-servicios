package com.marioCorps.carritooaut.service;

import com.marioCorps.carritocommons.model.Usuario;

public interface IUsuarioService {	
	Usuario findByUsername(String nombre);
	
	Usuario update(Usuario usuario, Long idUsuario);
	
}
