package com.marioCorps.carritooaut.service;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.marioCorps.carritocommons.model.Usuario;
import com.marioCorps.carritooaut.httpClient.UsuarioFeingClient;

import feign.FeignException;

@Service
public class UsuarioService implements UserDetailsService, IUsuarioService {

	@Autowired
	private UsuarioFeingClient usrClinetHttp;

	Logger logger = LoggerFactory.getLogger(UsuarioService.class);

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Usuario usuario = null;
		try {
			usuario = this.usrClinetHttp.findByUsername(username);
		} catch (FeignException e) {
			throw new UsernameNotFoundException("Error en el login");
		}

		List<GrantedAuthority> auths = usuario.getRoles().stream()
				.map(role -> new SimpleGrantedAuthority(role.getNombre()))
				.peek(auth -> logger.info("Auth: " + auth.getAuthority())).collect(Collectors.toList());

		return new User(usuario.getUsername(), usuario.getPassword(), usuario.isEnabled(), true, true, true, auths);
	}

	@Override
	public Usuario findByUsername(String nombre) {
		return this.usrClinetHttp.findByUsername(nombre);
	}

	@Override
	public Usuario update(Usuario usuario, Long idUsuario) {
		return this.usrClinetHttp.update(usuario, idUsuario);
	}
}
