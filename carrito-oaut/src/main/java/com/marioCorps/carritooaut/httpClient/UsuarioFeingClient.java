package com.marioCorps.carritooaut.httpClient;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.marioCorps.carritocommons.model.Usuario;

import feign.Body;

@FeignClient(name="carrito-clientes")
public interface UsuarioFeingClient {
	
	@GetMapping("/usaurios/search/filtrar")
	Usuario findByUsername(@RequestParam String nick);
	
	@PutMapping("/usaurios/{idUsuario}")
	Usuario update(@RequestBody Usuario usuario, @RequestParam Long idUsuario);
}
