package com.marioCorps.carritooaut.OAuthConfig.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationEventPublisher;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import com.marioCorps.carritocommons.model.Usuario;
import com.marioCorps.carritooaut.service.IUsuarioService;

import feign.FeignException;

@Component
public class AuthenticationEventHandler implements AuthenticationEventPublisher {
	Logger logger = LoggerFactory.getLogger(AuthenticationEventHandler.class);
	
	@Autowired
	private IUsuarioService usrService;
	
	@Override
	public void publishAuthenticationSuccess(Authentication authentication) {
		UserDetails user = (UserDetails)authentication.getPrincipal();
		logger.info("Success Usuario authenticado: " + user.getUsername());
		
		String username = authentication.getName();
		Usuario usr = this.usrService.findByUsername(username); 
		if(usr.getIntentos() == null) { usr.setIntentos(0); }
		
		if(usr.getIntentos() > 0) { 
			usr.setIntentos(0);
			logger.info(String.format("Numero de intentos de %s reiniciado", username)); 
			this.usrService.update(usr, usr.getId());
		}
	}

	@Override
	public void publishAuthenticationFailure(AuthenticationException exception, Authentication authentication) {
		logger.error("Error al autentificarse: " + exception.getMessage());
		
		//CONSEGUIMOS USUARIO SI EXISTE A PARTIR DEL USERNAME
		String username = authentication.getName();
		Usuario usr = null;
		try { usr = this.usrService.findByUsername(username); } 
		catch (FeignException e) { logger.error(String.format("El usuario %s no existe", username)); return ; }
		if(usr.getIntentos() == null) { usr.setIntentos(0); }
		
		//SE REGISTRA INTENTO FALLIDO
		usr.setIntentos(usr.getIntentos()+1);
		logger.warn(String.format("El usuario %s hizo intento %s de 3", username, usr.getIntentos()));
		
		//SI >=3 DE INTENTOS DESHABILITAMOS USUARIO Y GUARDAMOS
		if(usr.getIntentos() >= 3) { 
			usr.setEnabled(false);
			logger.error(String.format("El usuario %s fue inabilitado por numero de intentos", username)); 
		}
		//guardamos
		this.usrService.update(usr, usr.getId());
	}
}
